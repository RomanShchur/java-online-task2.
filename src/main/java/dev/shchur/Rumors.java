package dev.shchur;

import java.util.Scanner;

public class Rumors {
    /**
     * Main method of program
     *
     * @param args
     */
    public static void main(String[] args) {
        int SsatisticalSample = 200;
        int n = 0;
        usersInput(n);
        int countRumorGotFullSpread = 0;
        int peopleHeard = 0;
        for (int i = 0; i < SsatisticalSample; i++) {
            boolean guests[] = new boolean[n];
            guests[1] = true;
            boolean alreadyHeard = false;
            int nextPerson = -1;
            int currentPerson = 1;
            while (!alreadyHeard) {
                nextPersonGeneration(nextPerson, n, currentPerson);
                if (guests[nextPerson]) {
                    if (rumorSpreaded(guests)) {
                        countRumorGotFullSpread++;
                    }
                    peopleHeard = peopleHeard + PeopleReachedCounter(guests);
                    alreadyHeard = true;
                }
                guests[nextPerson] = true;
                currentPerson = nextPerson;
            }
        }
        System.out.println("Probability that everyone except Alice heared rumor  in " + SsatisticalSample + " attempts is " +
                (double) countRumorGotFullSpread / SsatisticalSample);
        System.out.println("Average amount of people that heared rumor is " + peopleHeard / SsatisticalSample);
    }

    /**
     * Method that counts how many people heard rumor
     *
     * @param arr
     * @return count people heard rumor count
     */
    public static int PeopleReachedCounter(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i]) {
                counter++;
            }
        return counter;
    }

    /**
     * Checks if all guests heard rumor
     *
     * @param arr
     * @return boolean if spread is complete
     */
    public static boolean rumorSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i]) {
                return false;
            }
        return true;
    }

    /**
     * Method generates random next person to whom will rumor be tolled now
     *
     * @param nextPerson
     * @param n
     * @param currentPerson
     * @return nextPerson generated randomly
     */
    public static int nextPersonGeneration(int nextPerson, int n, int currentPerson) {
        nextPerson = 1 + (int) (Math.random() * (n - 1));
        if (nextPerson == currentPerson) {
            while (nextPerson == currentPerson) {
                nextPerson = 1 + (int) (Math.random() * (n - 1));
            }
        }
        return nextPerson;
    }

    /**
     * Method that reads user input from console
     *
     * @param n
     * @return user input n
     */
    public static int usersInput(int n) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of guests:" + "\n" + "Number should be greater than 2");
        n = sc.nextInt();
        return n;
    }
}
